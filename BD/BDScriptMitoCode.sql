USE [mitocodedb]
GO
/****** Object:  Table [dbo].[consult]    Script Date: 29/04/2023 18:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[consult](
	[id_consult] [int] IDENTITY(1,1) NOT NULL,
	[consult_date] [datetime2](6) NOT NULL,
	[num_consult] [varchar](3) NOT NULL,
	[id_medic] [int] NOT NULL,
	[id_patient] [int] NOT NULL,
	[id_specialty] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_consult] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[consult_detail]    Script Date: 29/04/2023 18:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[consult_detail](
	[id_detail] [int] IDENTITY(1,1) NOT NULL,
	[diagnosis] [varchar](70) NOT NULL,
	[treatment] [varchar](300) NOT NULL,
	[id_consult] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_detail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[consult_exam]    Script Date: 29/04/2023 18:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[consult_exam](
	[id_consult] [int] NOT NULL,
	[id_exam] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_consult] ASC,
	[id_exam] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[exam]    Script Date: 29/04/2023 18:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[exam](
	[id_exam] [int] IDENTITY(1,1) NOT NULL,
	[description] [varchar](100) NOT NULL,
	[name] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_exam] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[media_file]    Script Date: 29/04/2023 18:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[media_file](
	[id_file] [int] IDENTITY(1,1) NOT NULL,
	[filename] [varchar](50) NOT NULL,
	[filetype] [varchar](20) NOT NULL,
	[content] [varbinary](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_file] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[medic]    Script Date: 29/04/2023 18:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[medic](
	[id_medic] [int] IDENTITY(1,1) NOT NULL,
	[cmp] [varchar](12) NOT NULL,
	[first_name] [varchar](70) NOT NULL,
	[last_name] [varchar](70) NOT NULL,
	[photo_url] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_medic] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[menu]    Script Date: 29/04/2023 18:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[menu](
	[id_menu] [int] NOT NULL,
	[icon] [varchar](20) NOT NULL,
	[name] [varchar](20) NOT NULL,
	[url] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_menu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[menu_role]    Script Date: 29/04/2023 18:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[menu_role](
	[id_menu] [int] NOT NULL,
	[id_role] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[patient]    Script Date: 29/04/2023 18:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[patient](
	[id_patient] [int] IDENTITY(1,1) NOT NULL,
	[address] [varchar](150) NULL,
	[dni] [varchar](8) NOT NULL,
	[email] [varchar](55) NOT NULL,
	[first_name] [varchar](70) NOT NULL,
	[last_name] [varchar](70) NOT NULL,
	[phone] [varchar](9) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_patient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[reset_mail]    Script Date: 29/04/2023 18:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reset_mail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[expiration] [datetime2](6) NOT NULL,
	[random] [varchar](255) NOT NULL,
	[id_user] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[role]    Script Date: 29/04/2023 18:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[role](
	[id_role] [int] NOT NULL,
	[description] [varchar](100) NOT NULL,
	[name] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_role] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[signos]    Script Date: 29/04/2023 18:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[signos](
	[id_signos] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [datetime2](6) NOT NULL,
	[pulso] [varchar](100) NULL,
	[ritmo] [varchar](100) NULL,
	[temperatura] [varchar](100) NULL,
	[id_patient] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_signos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[specialty]    Script Date: 29/04/2023 18:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[specialty](
	[id_specialty] [int] IDENTITY(1,1) NOT NULL,
	[description] [varchar](100) NOT NULL,
	[name] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_specialty] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_data]    Script Date: 29/04/2023 18:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_data](
	[id_user] [int] NOT NULL,
	[enabled] [bit] NOT NULL,
	[password] [varchar](60) NOT NULL,
	[username] [varchar](60) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_user] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_role]    Script Date: 29/04/2023 18:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_role](
	[id_user] [int] NOT NULL,
	[id_role] [int] NOT NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[consult] ON 

INSERT [dbo].[consult] ([id_consult], [consult_date], [num_consult], [id_medic], [id_patient], [id_specialty]) VALUES (1, CAST(N'2023-04-29T00:00:00.0000000' AS DateTime2), N'C1', 3, 1, 1)
SET IDENTITY_INSERT [dbo].[consult] OFF
GO
SET IDENTITY_INSERT [dbo].[consult_detail] ON 

INSERT [dbo].[consult_detail] ([id_detail], [diagnosis], [treatment], [id_consult]) VALUES (1, N'PRUEBA', N'PRUEBA2', 1)
SET IDENTITY_INSERT [dbo].[consult_detail] OFF
GO
SET IDENTITY_INSERT [dbo].[medic] ON 

INSERT [dbo].[medic] ([id_medic], [cmp], [first_name], [last_name], [photo_url]) VALUES (1, N'123456', N'CARLOS', N'ULLOA BENITES', N'')
INSERT [dbo].[medic] ([id_medic], [cmp], [first_name], [last_name], [photo_url]) VALUES (2, N'412536', N'RODRIGO', N'AVILA ULLOA', N'')
INSERT [dbo].[medic] ([id_medic], [cmp], [first_name], [last_name], [photo_url]) VALUES (3, N'95959', N'FRESIA', N'MURGUIA VILCHEZ', NULL)
SET IDENTITY_INSERT [dbo].[medic] OFF
GO
INSERT [dbo].[menu] ([id_menu], [icon], [name], [url]) VALUES (1, N'home', N'Dashboard', N'/pages/dashboard')
INSERT [dbo].[menu] ([id_menu], [icon], [name], [url]) VALUES (2, N'search', N'Search', N'/pages/search')
INSERT [dbo].[menu] ([id_menu], [icon], [name], [url]) VALUES (3, N'insert_drive_file', N'Consult', N'/pages/consult')
INSERT [dbo].[menu] ([id_menu], [icon], [name], [url]) VALUES (4, N'insert_drive_file', N'Consult Autocomplete', N'/pages/consult-autocomplete')
INSERT [dbo].[menu] ([id_menu], [icon], [name], [url]) VALUES (5, N'view_carousel', N'Consult Wizard', N'/pages/consult-wizard')
INSERT [dbo].[menu] ([id_menu], [icon], [name], [url]) VALUES (6, N'star_rate', N'Specialties', N'/pages/specialty')
INSERT [dbo].[menu] ([id_menu], [icon], [name], [url]) VALUES (7, N'healing', N'Medics', N'/pages/medic')
INSERT [dbo].[menu] ([id_menu], [icon], [name], [url]) VALUES (8, N'assignment', N'Exams', N'/pages/exam')
INSERT [dbo].[menu] ([id_menu], [icon], [name], [url]) VALUES (9, N'accessibility', N'Patients', N'/pages/patient')
INSERT [dbo].[menu] ([id_menu], [icon], [name], [url]) VALUES (10, N'assessment', N'Reports', N'/pages/report')
INSERT [dbo].[menu] ([id_menu], [icon], [name], [url]) VALUES (11, N'enhanced_encryption', N'Signos Vitales', N'/pages/signos')
INSERT [dbo].[menu] ([id_menu], [icon], [name], [url]) VALUES (12, N'assignment_ind', N'Perfil', N'/pages/perfil')
GO
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (1, 1)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (2, 1)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (3, 1)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (4, 1)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (5, 1)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (6, 1)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (7, 1)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (8, 1)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (9, 1)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (10, 1)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (1, 2)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (3, 2)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (4, 2)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (5, 2)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (6, 2)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (11, 1)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (11, 2)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (12, 1)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (12, 2)
INSERT [dbo].[menu_role] ([id_menu], [id_role]) VALUES (9, 2)
GO
SET IDENTITY_INSERT [dbo].[patient] ON 

INSERT [dbo].[patient] ([id_patient], [address], [dni], [email], [first_name], [last_name], [phone]) VALUES (1, NULL, N'46464646', N'mia@gmail.com', N'MÍA', N'ULLOA PEÑA', N'989898657')
INSERT [dbo].[patient] ([id_patient], [address], [dni], [email], [first_name], [last_name], [phone]) VALUES (2, NULL, N'63465646', N'dante@gmail.com', N'DANTE', N'ULLOA CONDORI', N'969228657')
INSERT [dbo].[patient] ([id_patient], [address], [dni], [email], [first_name], [last_name], [phone]) VALUES (3, NULL, N'23464746', N'lucia@gmail.com', N'LUCIA', N'BENDREL PAUCAR', N'981198227')
INSERT [dbo].[patient] ([id_patient], [address], [dni], [email], [first_name], [last_name], [phone]) VALUES (4, N'BARRANCA', N'46464646', N'MAU@GMAIL.COM', N'MAU', N'AMADO', N'332323232')
INSERT [dbo].[patient] ([id_patient], [address], [dni], [email], [first_name], [last_name], [phone]) VALUES (5, NULL, N'46464688', N'susy@gmail.com', N'SUSY ', N'ULLOA', N'989898555')
INSERT [dbo].[patient] ([id_patient], [address], [dni], [email], [first_name], [last_name], [phone]) VALUES (29, NULL, N'12345678', N'UNO@GMAIL.COM', N'UNO', N'UNO', N'123456789')
INSERT [dbo].[patient] ([id_patient], [address], [dni], [email], [first_name], [last_name], [phone]) VALUES (30, NULL, N'12345678', N'sandy@gmail.com', N'SANDY', N'MURGUIA', N'123456789')
INSERT [dbo].[patient] ([id_patient], [address], [dni], [email], [first_name], [last_name], [phone]) VALUES (31, NULL, N'12345678', N'JAIME@GMAIL.COM', N'JAIME', N'MEDINA', N'123456789')
SET IDENTITY_INSERT [dbo].[patient] OFF
GO
INSERT [dbo].[role] ([id_role], [description], [name]) VALUES (1, N'Administrador', N'ADMIN')
INSERT [dbo].[role] ([id_role], [description], [name]) VALUES (2, N'Usuario', N'USER')
INSERT [dbo].[role] ([id_role], [description], [name]) VALUES (3, N'Admin de bd', N'DBA')
GO
SET IDENTITY_INSERT [dbo].[signos] ON 

INSERT [dbo].[signos] ([id_signos], [fecha], [pulso], [ritmo], [temperatura], [id_patient]) VALUES (1, CAST(N'2023-04-29T17:29:27.0000000' AS DateTime2), N'2222', N'33333', N'111112', 5)
INSERT [dbo].[signos] ([id_signos], [fecha], [pulso], [ritmo], [temperatura], [id_patient]) VALUES (2, CAST(N'2023-04-29T18:06:33.0000000' AS DateTime2), N'211', N'311', N'111', 30)
INSERT [dbo].[signos] ([id_signos], [fecha], [pulso], [ritmo], [temperatura], [id_patient]) VALUES (3, CAST(N'2023-04-29T18:25:19.0000000' AS DateTime2), N'123', N'124', N'122', 31)
SET IDENTITY_INSERT [dbo].[signos] OFF
GO
SET IDENTITY_INSERT [dbo].[specialty] ON 

INSERT [dbo].[specialty] ([id_specialty], [description], [name]) VALUES (1, N'MEDICO', N'PEDIATRICO')
INSERT [dbo].[specialty] ([id_specialty], [description], [name]) VALUES (2, N'MEDICO', N'CARDIOLOG')
INSERT [dbo].[specialty] ([id_specialty], [description], [name]) VALUES (3, N'MEDICO', N'CIRUJANO')
INSERT [dbo].[specialty] ([id_specialty], [description], [name]) VALUES (4, N'MEDICO', N'GENERAL')
SET IDENTITY_INSERT [dbo].[specialty] OFF
GO
INSERT [dbo].[user_data] ([id_user], [enabled], [password], [username]) VALUES (1, 1, N'$2a$10$ju20i95JTDkRa7Sua63JWOChSBc0MNFtG/6Sps2ahFFqN.HCCUMW.', N'mitotest21@gmail.com')
INSERT [dbo].[user_data] ([id_user], [enabled], [password], [username]) VALUES (2, 1, N'$2a$10$ju20i95JTDkRa7Sua63JWOChSBc0MNFtG/6Sps2ahFFqN.HCCUMW.', N'mitocode21@gmail.com')
GO
INSERT [dbo].[user_role] ([id_user], [id_role]) VALUES (1, 1)
INSERT [dbo].[user_role] ([id_user], [id_role]) VALUES (1, 3)
INSERT [dbo].[user_role] ([id_user], [id_role]) VALUES (2, 2)
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UK_58c7v94ej3oi2qfm7s2xygiml]    Script Date: 29/04/2023 18:47:34 ******/
ALTER TABLE [dbo].[reset_mail] ADD  CONSTRAINT [UK_58c7v94ej3oi2qfm7s2xygiml] UNIQUE NONCLUSTERED 
(
	[random] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UK_nlc4atex50p892vsfhwccm336]    Script Date: 29/04/2023 18:47:34 ******/
ALTER TABLE [dbo].[user_data] ADD  CONSTRAINT [UK_nlc4atex50p892vsfhwccm336] UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[consult]  WITH CHECK ADD  CONSTRAINT [FK_CONSULT_MEDIC] FOREIGN KEY([id_medic])
REFERENCES [dbo].[medic] ([id_medic])
GO
ALTER TABLE [dbo].[consult] CHECK CONSTRAINT [FK_CONSULT_MEDIC]
GO
ALTER TABLE [dbo].[consult]  WITH CHECK ADD  CONSTRAINT [FK_CONSULT_PATIENT] FOREIGN KEY([id_patient])
REFERENCES [dbo].[patient] ([id_patient])
GO
ALTER TABLE [dbo].[consult] CHECK CONSTRAINT [FK_CONSULT_PATIENT]
GO
ALTER TABLE [dbo].[consult]  WITH CHECK ADD  CONSTRAINT [FK_CONSULT_SPECIALTY] FOREIGN KEY([id_specialty])
REFERENCES [dbo].[specialty] ([id_specialty])
GO
ALTER TABLE [dbo].[consult] CHECK CONSTRAINT [FK_CONSULT_SPECIALTY]
GO
ALTER TABLE [dbo].[consult_detail]  WITH CHECK ADD  CONSTRAINT [FK_CONSULT_DETAIL] FOREIGN KEY([id_consult])
REFERENCES [dbo].[consult] ([id_consult])
GO
ALTER TABLE [dbo].[consult_detail] CHECK CONSTRAINT [FK_CONSULT_DETAIL]
GO
ALTER TABLE [dbo].[consult_exam]  WITH CHECK ADD  CONSTRAINT [FK591nrrtt1rqpma2ax5f0u1rr] FOREIGN KEY([id_consult])
REFERENCES [dbo].[consult] ([id_consult])
GO
ALTER TABLE [dbo].[consult_exam] CHECK CONSTRAINT [FK591nrrtt1rqpma2ax5f0u1rr]
GO
ALTER TABLE [dbo].[consult_exam]  WITH CHECK ADD  CONSTRAINT [FKjmghcc7tn12i5uav4lisb127t] FOREIGN KEY([id_exam])
REFERENCES [dbo].[exam] ([id_exam])
GO
ALTER TABLE [dbo].[consult_exam] CHECK CONSTRAINT [FKjmghcc7tn12i5uav4lisb127t]
GO
ALTER TABLE [dbo].[menu_role]  WITH CHECK ADD  CONSTRAINT [FK2ymscnycm83uqu1ddpkgdryg1] FOREIGN KEY([id_menu])
REFERENCES [dbo].[menu] ([id_menu])
GO
ALTER TABLE [dbo].[menu_role] CHECK CONSTRAINT [FK2ymscnycm83uqu1ddpkgdryg1]
GO
ALTER TABLE [dbo].[menu_role]  WITH CHECK ADD  CONSTRAINT [FKbhl7xy7xjv54q9vedxuxk2kn0] FOREIGN KEY([id_role])
REFERENCES [dbo].[role] ([id_role])
GO
ALTER TABLE [dbo].[menu_role] CHECK CONSTRAINT [FKbhl7xy7xjv54q9vedxuxk2kn0]
GO
ALTER TABLE [dbo].[reset_mail]  WITH CHECK ADD  CONSTRAINT [FKpcrxy7l7u4gplc15wuqp02o1a] FOREIGN KEY([id_user])
REFERENCES [dbo].[user_data] ([id_user])
GO
ALTER TABLE [dbo].[reset_mail] CHECK CONSTRAINT [FKpcrxy7l7u4gplc15wuqp02o1a]
GO
ALTER TABLE [dbo].[signos]  WITH CHECK ADD  CONSTRAINT [FK_VITALSIGNS_PATIENT] FOREIGN KEY([id_patient])
REFERENCES [dbo].[patient] ([id_patient])
GO
ALTER TABLE [dbo].[signos] CHECK CONSTRAINT [FK_VITALSIGNS_PATIENT]
GO
ALTER TABLE [dbo].[user_role]  WITH CHECK ADD  CONSTRAINT [FK2aam9nt2tv8vcfymi3jo9c314] FOREIGN KEY([id_role])
REFERENCES [dbo].[role] ([id_role])
GO
ALTER TABLE [dbo].[user_role] CHECK CONSTRAINT [FK2aam9nt2tv8vcfymi3jo9c314]
GO
ALTER TABLE [dbo].[user_role]  WITH CHECK ADD  CONSTRAINT [FK9qphm07p6th93qb8p9fnykvwo] FOREIGN KEY([id_user])
REFERENCES [dbo].[user_data] ([id_user])
GO
ALTER TABLE [dbo].[user_role] CHECK CONSTRAINT [FK9qphm07p6th93qb8p9fnykvwo]
GO
