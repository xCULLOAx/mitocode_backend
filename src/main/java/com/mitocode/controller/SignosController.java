package com.mitocode.controller;

import com.mitocode.dto.PatientDTO;
import com.mitocode.dto.SignosDTO;
import com.mitocode.model.Patient;
import com.mitocode.model.Signos;
import com.mitocode.service.impl.PatientServiceImpl;
import com.mitocode.service.impl.SignosServiceImpl;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;


@RestController
@RequestMapping("/vsignos")
@RequiredArgsConstructor
//@CrossOrigin(origins = "http://localhost:4200")
public class SignosController {

    //@Autowired
    private final SignosServiceImpl service;

    @Qualifier("defaultMapper")
    private final ModelMapper mapper;

    @GetMapping
    public ResponseEntity<List<SignosDTO>> findAll(){
        System.out.println("SIGNOOOOS findAll::: ");
        List<SignosDTO> list = service.findAll().stream().map(this::convertToDto).collect(Collectors.toList());
        return new ResponseEntity<>(list, OK);
    }


    @GetMapping("/{id}")
    public ResponseEntity<SignosDTO> findById(@PathVariable("id") Integer id){
        System.out.println("SIGNOOOOS findById::: " +id);
        Signos obj = service.findById(id);
        return new ResponseEntity<>(this.convertToDto(obj), OK);
    }


    @PostMapping
    public ResponseEntity<Void> save(@Valid @RequestBody SignosDTO dto){
        System.out.println("SIGNOOOOS save::: " +dto);
        Signos obj = service.save(convertToEntity(dto));
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdSignos()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}")//(consumes = "", produces = "")
    public ResponseEntity<SignosDTO> update(@PathVariable("id") Integer id, @Valid @RequestBody SignosDTO dto){
        System.out.println("SIGNOOOOS update::: " +id + " DTO:: " +dto);
        dto.setIdSignos(id);
        Signos obj = service.update(convertToEntity(dto), id);
        return new ResponseEntity<>(convertToDto(obj), OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
        System.out.println("SIGNOOOOS delete::: " +id);
        service.delete(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

//    @GetMapping("/hateoas/{id}")
//    public EntityModel<SignosDTO> findByIdHateoas(@PathVariable("id") Integer id){
//        EntityModel<SignosDTO> resource = EntityModel.of(this.convertToDto(service.findById(id)));
//        WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).findById(id));
//        WebMvcLinkBuilder link2 = linkTo(methodOn(LanguageController.class).changeLocale("EN"));
//        resource.add(link1.withRel("patient-info1"));
//        resource.add(link2.withRel("language-info"));
//
//        return resource;
//    }

    @GetMapping("/pageable")
    public ResponseEntity<Page<SignosDTO>> listPage(Pageable pageable){
        Page<SignosDTO> page = service.listPage(pageable).map(p -> mapper.map(p, SignosDTO.class));
        return new ResponseEntity<>(page, OK);
    }

    private SignosDTO convertToDto(Signos obj){
        return mapper.map(obj, SignosDTO.class);
    }

    private Signos convertToEntity(SignosDTO dto){
        return mapper.map(dto, Signos.class);
    }


}
