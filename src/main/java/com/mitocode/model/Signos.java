package com.mitocode.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Signos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Integer idSignos;

    @ManyToOne
    @JoinColumn(name = "id_patient", nullable = false, foreignKey = @ForeignKey(name = "FK_VITALSIGNS_PATIENT"))
    private Patient patient;

    @Column(nullable = false) //ISODate yyyy-mm-ddTHH:mm:ss
    private LocalDateTime fecha;
    @Column(length = 100)
    private String temperatura;

    @Column(length = 100)
    private String pulso;

    @Column(length = 100)
    private String ritmo;
}