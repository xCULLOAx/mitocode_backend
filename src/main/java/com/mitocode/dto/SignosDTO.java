package com.mitocode.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class SignosDTO {

    @EqualsAndHashCode.Include
    private Integer idSignos;

    @NotNull
    private PatientDTO patient;

    @NotNull
    private LocalDateTime fecha = LocalDateTime.now();
    private String temperatura;

    private String pulso;

    private String ritmo;


}
